const ErrorHandler = require("../utils/errorhandler")

module.exports =(err,req,res,next) =>{
    err.statusCode = err.statusCode || 500
    err.message = err.message || "Internal server error";

    //wrong mongo id cast error 
    if(err.name === "CastError"){
        const message =`Resource not found,Invalid:${err.path}`;
        err=new ErrorHandler(message,400);
    }

    //Duplicate key error 
    if(err.code === 11000 ){
        const message = `duplicate ${object.keys(err.keyValue)} entered `
        err=new ErrorHandler(message,400);

    }

    res.status(err.statusCode).json({
        success:true,
        message:err.message,
    })
}



exports.module = ErrorHandler