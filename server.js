const app = require("./app")
const dotenv = require("dotenv")
const connectDatabase = require('./database')
//Handling uncaught exception

process.on("uncaughtException",(err)=>{
      console.log(`Error:${err.message}`)
      console.log(`Shutting down the server due to uncaught exception`)
      process.exit(1)
})

dotenv.config({path:"config.env"});
//let PORT =4000;

connectDatabase()
const server =app.listen(process.env.PORT,()=>{
     console.log(`server is working on http://localhost:${process.env.PORT}`)
})

//unhandled rejections

process.on("unhandledRejection",err =>{
     console.log(`Error:${err.message}`);
     console.log(`Shutting down the server due to unhandled promise rejection`)
     server.close(()=>{
          process.exit(1)
     })
})