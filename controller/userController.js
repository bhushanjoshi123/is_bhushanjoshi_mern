const Errorhandler = require("../utils/errorhandler")
const User = require("../models/usermodels");
const sendToken = require("../utils/jwttoken");
const catchAsyncError = require("../middleware/catchAsyncError");
const sendEmail = require("../utils/sendEmail.js")
exports.registerUser = async (req, res, next) => {
    const { name, email, password } = req.body;
    const user = await User.create({
        name, email, password,
        avatar: {
            public_id:
                "this is a sample id",
            url:
                "profitepicUrl"

        }
    })

    sendToken(user, 200, res)
}
//Login User 
exports.loginUser = async (req, res, next) => {
    const { email, password } = req.body;

    //checking if user has given password and email both
    if (!email || !password) {
        return next(new Errorhandler("please enter email and password", 401))
    }
    const user = await User.findOne({ email }).select("+password");

    if (!user) {
        return next(new Errorhandler("Invalid email or password", 401));
    }
    const isPasswordMatched = user.comparePassword(password);

    if (!isPasswordMatched) {
        return next(new Errorhandler("Invalid email or password", 401));
    }

    sendToken(user, 200, res);

}

exports.logout = async (req,res,next) =>{
    res.cookie("token",null, {
        expires:new Date(Date.now()),
        httpOnly:true
    })
   
    res.status(200).json({
        success:true,
        message:"Logout sucessfully"
    })
}

//Forget password

exports.forgotPassword  = catchAsyncError(async (req,res,next) =>{
    const user = await User.findOne({email:req.body.email});
    if(!user){
        return next(new Errorhandler("user not found",404))
    }
    //get reset password token
    const resetToken = user.getResetPasswordToken();
    await user.save({validateBeforeSave: false});

    const resetPasswordUrl = `${req.protocol}://${req.get("host")}/api/v1/password/reset/${resetToken}`
    const message = `your password reset token is :- \n \n ${resetPasswordUrl} \n\n If you have not requested this email then please ignore it`;
    
    try {

        await sendEmail({
            email:user.email,
            subject:`Ecommerce Password recovery`,
            message,

        });
        res.status(200).json({
            success:true,
            message:`Email sent to ${user.email} successfully `,

        })

    }catch(error){
    user.resetPasswordToken = undefined
    user.resetPasswordExpire = undefined
    await user.save({validateBeforeSave: false});
        return next(new Errorhandler(error.message, 500))

    }
})