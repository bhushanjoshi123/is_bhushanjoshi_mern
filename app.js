const express =require("express")
const app =express()
const errorMiddleware=require("./middleware/error")
const cookieParser = require("cookie-parser")
const bodyParser = require("body-parser")

app.use(cookieParser())
app.use(express.urlencoded({ extended : true }));
app.use(bodyParser.urlencoded({extended: true}))
app.use(express.json())


//route Imports
const product =require("./routes/productRoute.js")
const user = require("./routes/userRoutes")
app.use("/api/v1",product);
app.use("/api/v1",user)
//middleware for error
app.use(errorMiddleware)

module.exports = app;