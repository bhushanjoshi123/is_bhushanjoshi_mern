const express =require("express")
const router = express.Router()

const { loginUser,registerUser, logout  } = require("../controller/userController");

router.route("/login").post(loginUser)
router.route("/register").post(registerUser);
router.route("/logout").get(logout)

module.exports = router
